# coding = utf-8
from flask_sqlalchemy import SQLAlchemy, declarative_base

db = SQLAlchemy()
db_base = declarative_base()

